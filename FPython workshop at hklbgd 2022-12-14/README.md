# Whether python treats functions as First-Class Citizen?

>Yes, Python treats functions as first-class citizens, which means that functions in Python have the ***same rights and privileges*** as any other type of value, such as integers, strings, or objects.
>> ***OpenAI's gpt3 chat bot***

>In functional programming, functions are treated as [first-class citizens](https://en.wikipedia.org/wiki/First-class_citizen "First-class citizen"), meaning that they can be bound to names (including local [identifiers](https://en.wikipedia.org/wiki/Identifier_(computer_languages) "Identifier (computer languages)")), passed as [arguments](https://en.wikipedia.org/wiki/Parameter_(computer_programming) "Parameter (computer programming)"), and [returned](https://en.wikipedia.org/wiki/Return_value "Return value") from other functions, just as any other [data type](https://en.wikipedia.org/wiki/Data_type "Data type") can. This allows programs to be written in a [declarative](https://en.wikipedia.org/wiki/Declarative_programming "Declarative programming") and [composable](https://en.wikipedia.org/wiki/Composability "Composability") style, where small functions are combined in a [modular](https://en.wikipedia.org/wiki/Modular_programming "Modular programming") manner.
>> ***Wikipedia*** 

- Python is not a "pure" functional language like Haskell
- but enables some but not all (e.g. tail recursion) FP concepts and methods
- which can be combined with imperative programming


```python
#import all! :D
!python3 -m pip install more-itertools  > /dev/null && echo ':)' || echo ':('
from functools import *
from itertools import *
from more_itertools import *
from operator import *
from typing import *
from collections import Counter
import sys, math
```

    :)



```python
# A function that takes a function as an argument and returns a new function
def apply_func(func, arg):
    return func(arg)

# A function that returns the square of a number
def square(x):
    return x * x

# A function that returns the cube of a number
def cube(x):
    return x * x * x

# Using the apply_func() function to apply the square() function to the number 2
squared = apply_func(square, 2)
print(squared) # Output: 4

# Using the apply_func() function to apply the cube() function to the number 2
cubed = apply_func(cube, 2)
print(cubed) # Output: 8
```

    4
    8



```python
def f(): 
    print('Hello')
    
f.some_field = 5

f() # Output: Hello

print(f.some_field ) # Output: 5
```

    Hello
    5



```python
class f:
    def __call__(self):
        print('Hello')
f = f()
```

# Lambda functions 


```python
def add(a,b):
    return a+b

add = lambda a,b: a+b
```


```python
def getfunc(args, expr):
    exec('def f(' + args + '): return ' + expr)
    return eval('f')
```


```python
# Define a function that takes two arguments (x and y) and returns their sum
sum_function = getfunc('x, y', 'x + y')

# Now the sum_function() function can be called with two arguments (x and y) and will return their sum
print(sum_function(2, 3)) # Output: 5
```

    5


# What is funtool lib

## Does the python language supports currying?


```python
# A function that takes two arguments (x and y) and returns their sum
def add(x, y):
    return x + y

# Using the partial() function from the functools module to curry the add() function
from functools import partial
add_five = partial(add, 5)

# Now the add_five function can be called with just one argument (y)
print(add_five(3)) # Output: 8
```

    8


```python
min_student = partial(min, key=lambda student: median(student.test_scores))
min_student(studens)
```


```python
add_five = lambda b: add(5,b)

def curry_add_x(x): #*arg, **kw):
    def add_x(y):
        return add(x,y)
    return add_x 

add_five = curry_add_x(5)

print(add_five(3)) # Output: 8
```

    8


#### Partial Method (functools) 


```python
class Student:
    
    def __init__(on_vacation=False):
        self._on_vacation = on_vacation
    
    @property
    def on_vacation(self):
        return self._on_vacation
        
    def set_vacation_status(self, status:bool):
        print(f'Setting on_vacation to {status}')
        self._on_vacation = status
    
    back_from_hol = partialmethod(set_vacation_status, False)
    on_hol = partialmethod(set_vacation_status, True)
```

## Map


```python
# A function that returns the square of a number
def square(x):
    return x * x

# A list of numbers to apply the square() function to
numbers = [1, 2, 3, 4, 5]

# Using the map() function to apply the square() function to each item in the list of numbers
squared_numbers = map(square, numbers)
print(list(squared_numbers)) # Output: [1, 4, 9, 16, 25]
```

    [1, 4, 9, 16, 25]



```python
squared_numbers = (square(x) for x in numbers)

squared_numbers = [square(x) for x in numbers]
```


```python
numbers = ["1010", "1011", "1100", "1101"]

integers = map(lambda x: int(x, base=2), numbers)
print(list(integers))

integers = (int(x, base=2) for x in numbers)
print(list(integers))

integers = [int(x, base=2) for x in numbers]
print(integers)
```

    [10, 11, 12, 13]
    [10, 11, 12, 13]
    [10, 11, 12, 13]


## Filter


```python
# A function that checks if a number is even
def is_even(x):
    return x % 2 == 0

# A list of numbers to filter
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# Using the filter() function to get only the even numbers from the list
even_numbers = filter(is_even, numbers)
print(list(even_numbers)) # Output: [2, 4, 6, 8, 10]
```

    [2, 4, 6, 8, 10]



```python
even_numbers = (x for x in numbers if is_even(x))
```


```python
filter, filterfalse, filter_except
```




    (filter,
     itertools.filterfalse,
     <function more_itertools.more.filter_except(validator, iterable, *exceptions)>)




```python
def f(a:str) -> int:
    return a+1

a: int = 1
f(a)
```




    2



## Named Tuple


```python
# Import the namedtuple() function from the functools module
from functools import namedtuple
# from collections import nametuple
# from typing import NamedTuple

# Create a namedtuple class for a person with fields for name, age, and gender
Person = namedtuple('Person', ['name', 'age', 'gender'])

# Create an instance of the Person namedtuple
jane = Person('Jane', 28, 'female')

# Access the fields of the Person instance using dot notation
print(jane.name) # Output: "Jane"
print(jane.age) # Output: 28
print(jane.gender) # Output: "female"
```

    Jane
    28
    female



```python
red = lambda color: color[0]
green = lambda color: color[1]
blue = lambda color: color[2]
```


```python
from typing import Tuple, Callable
RGB = Tuple[int,int,int]
red: Callable[[RGB],int] = lambda color: color[0]
```


```python
from collections import namedtuple
#or
from functools import namedtuple

Color = namedtuple("Color",("red","green","blue", "name"))

piksel1 = Color(255,255,255,'bela')
sys.getsizeof(piksel1), piksel1
```




    (72, Color(red=255, green=255, blue=255, name='bela'))




```python
from typing import NamedTuple

class Color(NamedTuple):
    red: int
    green: int
    blue: int
    name: str

piksel2 = Color(255,255,255,'bela')
sys.getsizeof(piksel2), piksel2._asdict()
```




    (72, {'red': 255, 'green': 255, 'blue': 255, 'name': 'bela'})




```python
class Color:
    def __init__(self,red,green,blue,name):
        self.red = red
        self.green = green
        self.blue = blue
        self.name = name
        
sys.getsizeof(Color(255,255,255,'bela'))
```




    48




```python
from typing import Optional
class Color(namedtuple('Color','red green blue name')):
    def __new__(cls,red, green, blue, name:Optional[str]=None):
        def check(col):
            assert isinstance(col, int), 'color must be int'
            if not (0 <= col <= 255):
                raise ValueError('color must be in [0,255] INTerval')
        for col in (red,green,blue):
            check(col)
        return super().__new__(cls,red, green, blue, name)
    
Color(0,0,0)
```




    Color(red=0, green=0, blue=0, name=None)




```python
class Color:
    __slots__ = ['red','__red', '__green', '__blue', '__name']
    
    def __init__(self,red,green,blue,name):
        self.__red = red
        self.__green = green
        self.__blue = blue
        self.__name = name
        
    def __getattr__(self, name):
        if name == 'red': return self.__red
        if name == 'green': return self.__green
        if name == 'blue': return self.__blue
        if name == 'name': return self.__name
        return None
        
piksel3 = Color(255,255,255,'bela')
piksel3._Color__red = 1
piksel3.red
sys.getsizeof(piksel3)
```




    72



# Decorators


```python
# A decorator function that adds a "!" to the end of a given function's return value
def exclaim(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return f"{result}!"
    return wrapper

# A simple function that returns a string
# @exclaim # we can just uncomment this line
def greet(name):
    return f"Hello, {name}"

# Using the exclaim() decorator to modify the behavior of the greet() function
greet_excited = exclaim(greet)

# Now the greet_excited() function will add a "!" to the end of the greet() function's return value
print(greet_excited("Jane")) # Output: "Hello, Jane!"
```

    Hello, Jane!


# Reduce (functools)


```python
from functools import reduce
from operator import mul

def factorialHOF(n):
    return reduce(mul, range(1, n+1))
```

```python
# statement-based while loop
while <cond>:
    <pre-suite>
    if <break_condition>:
        break
    else:
        <suite>
        
# FP-style recursive while loop
def while_block():
    <pre-suite>
    if <break_condition>:
        return 1
    else:
        <suite>
    return 0

while_FP = lambda: (<cond> and while_block()) or while_FP()
while_FP()
```


```python
from functools import namedtuple
from functools import reduce
from functools import partial

# Create a namedtuple class for a person with fields for name, age, and gender
Person = namedtuple('Person', ['name', 'age', 'gender'])

# A list of Person instances
people = [
    Person('Jane', 28, 'female'),
    Person('John', 30, 'male'),
    Person('Mike', 35, 'male'),
    Person('Lisa', 25, 'female')
]

# A function that returns the average age of a list of Person instances
def average_age(people):
    # Use the map() function and a lambda expression to get a list of the ages of the given people
    ages = map(lambda p: p.age, people)

    # Use the reduce() function and a lambda expression to calculate the sum of the ages
    total_age = reduce(lambda a, b: a + b, ages)

    # Return the average age, which is the total age divided by the number of people
    return total_age / len(people)

# A curried version of the average_age() function that takes the list of people as a partial argument
curried_average_age = partial(average_age, people)

# A decorator function that adds the string "The average age is: " to the beginning of a given function's return value
def add_text(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        return f"The average age is: {result}"
    return wrapper

# Using the add_text() decorator to modify the behavior of the curried_average_age() function
average_age_with_text = add_text(curried_average_age)

# Now the average_age_with_text() function will return the average age with the added text
print(average_age_with_text()) # Output: "The average age is: 29.25"
```

    The average age is: 29.5



```python
reduce((lambda acc,x: acc+[acc[-1]+x]),'abcde', [''])
```




    ['', 'a', 'ab', 'abc', 'abcd', 'abcde']



# Accumulate (itertools)


```python
list(accumulate('abcde',initial=''))
```




    ['', 'a', 'ab', 'abc', 'abcd', 'abcde']



# Example: Root


```python
# def root_iteracija(n, x):
#     return (x + n/x ) / 2

root_iteracija = lambda n, x: (x + n/x ) / 2

koren_iz_2_iter = lambda x: root_iteracija(2, x)
koren_iz_2_iter = lambda *arg: root_iteracija(2, *arg)
koren_iz_2_iter = lambda *arg,**kw: root_iteracija(2, *arg,**kw)

koren_iz_2_iter = partial(root_iteracija, 2)

f = koren_iz_2_iter
acc = 1.0

for _ in range(4):
    print(round(acc,6))
    acc = f(acc)
```

    1.0
    1.5
    1.416667
    1.414216



```python
def root_iterator(n, acc=1.0):
    yield acc
    yield from root_iterator(n, root_iteracija(n,acc))

# def root_iterator(n, acc=1.0):
#     while True:
#         yield (acc := root_iteracija(n, acc))

# [a for a,_ in zip(root_iterator(2),range(4))] #[-1]

# list(islice(root_iterator(2),4)) #[-1] 

#next(islice(root_iterator(2),4, None))

nth(root_iterator(2),10)
```




    1.414213562373095



# Pairwise (itertools)


```python
def root(n, ε=1e-15):
    return next(
        dropwhile(lambda x: abs(x[0]-x[1]) > ε, pairwise(root_iterator(n)))
    )[1]

print(*(f'koren od {i} je {root(i)}' for i in range(1,10)), sep='\n')
```

    koren od 1 je 1.0
    koren od 2 je 1.414213562373095
    koren od 3 je 1.7320508075688772
    koren od 4 je 2.0
    koren od 5 je 2.23606797749979
    koren od 6 je 2.449489742783178
    koren od 7 je 2.6457513110645907
    koren od 8 je 2.82842712474619
    koren od 9 je 3.0

```python
pairwise(list) == zip(list,list[1:])
wise2 = zip(list[0::2], list[1::2])
wise = zip(*(flat[i::n] for i in range(n)))
enumurate == zip(range(len(list)), list)
enumurate == zip(count(), list)

```
The nwise function in Python is a utility function that returns an iterator that produces tuples of n elements from an iterable. It uses the zip function to group the elements in the iterable by their indices, with n number of elements per group.

For example, if you have a list of elements [1, 2, 3, 4, 5, 6], and you call nwise on this list with n set to 3, the resulting iterator would produce tuples of three elements, like this:
```python
(1, 2, 3)
(2, 3, 4)
(3, 4, 5)
(4, 5, 6)
```

```python
nwise = lambda g, *, n=2: zip(*(islice(g,i, None) for i,g in enumerate(tee(g,n))))
nwise_longest = lambda g, *, n=2, fv=object(): zip_longest(*(islice(g,i, None) for i,g in enumerate(tee(g,n))), fv=fv)
first = lambda g, *, n=1: zip(chain(repeat(True,n), repeat(False)),g)
# last = lambda g, *, n=1: zip(chain(repeat(True,n), repeat(False)),g)

# for is_first, (is_last, x) in first(last('abc')):
for is_first, x in first('abc'):
    if not is_first:
        print(x)
#     if is_last:
#         pass
# All / Any

```

```python
da_li_je_broj_prost = lambda n: all(n%p for p in range(2,1+int(math.sqrt(n))))
da_li_je_broj_prost = lambda n: not any(not n%p for p in range(2,1+int(math.sqrt(n))))
```


```python
all([]), any([])
```




    (True, False)



# Recusion limit

### example: prime


```python
def is_prime(n):

    def is_prime_re(k,coprime):
        if k%coprime == 0:
            return False
        if k < coprime**2:
            return True
        return is_prime_re(k,coprime+2)
    
    if n < 2: 
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    return is_prime_re(n, 3)
```


```python
1_000_000
```




    1000000




```python
5_1
```




    51




```python
try:
    is_prime(62_710_561)
except RecursionError as e:
    print(e)
```

    maximum recursion depth exceeded in comparison



```python
sys.getrecursionlimit()
```




    3000




```python
sys.setrecursionlimit(3333)
sys.getrecursionlimit()
```




    3333



there is no tail recursion optimization:
- python is interpreted languages, so it cannot be optimized for tail recursion without losing some data for stuck track
- potentional compatibility with some functions which assume no tail recursion optimisations (maybe some functions for working with files?)


```python
def first(predicate: Callable, collection: Iterable) -> Any:
    for x in collection:
        if predicate(x): return x

def isprimeh(x: int) -> bool:
    if x == 2: return True
    if x % 2 == 0: return False
    factor= first( 
        lambda n: x%n==0,
        range(3, int(math.sqrt(x)+.5)+1, 2))
    return factor is None
```


```python
def fibi(n: int) -> int:
    if n == 0: return 0
    if n == 1: return 1
    f_n2, f_n1 = 1, 1
    for _ in range(3, n+1):
        f_n2, f_n1 = f_n1, f_n2+f_n1
    return f_n1
```


```python
def prosti(limit=None): # derived from 
    if limit and limit < 2:
        return
    q, D = 2, dict()
    while True:
        if limit and q > limit:
            return
        if q not in D:
            yield q
            D[q * q] = [q]
        else:
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]
        q += 1
```


```python
je_prost = lambda number: all( number%i != 0 for i in range(2, int(number**.5)+1) )
```


```python
for i, p in enumerate(prosti(31)):
    print(f'broj {p} je {i+1}. prost broj manji od 100')
```

    broj 2 je 1. prost broj manji od 100
    broj 3 je 2. prost broj manji od 100
    broj 5 je 3. prost broj manji od 100
    broj 7 je 4. prost broj manji od 100
    broj 11 je 5. prost broj manji od 100
    broj 13 je 6. prost broj manji od 100
    broj 17 je 7. prost broj manji od 100
    broj 19 je 8. prost broj manji od 100
    broj 23 je 9. prost broj manji od 100
    broj 29 je 10. prost broj manji od 100
    broj 31 je 11. prost broj manji od 100



```python
gener = (x for x in range(20,31) if je_prost(x))
next(gener)
```




    23




```python
next(gener)
```




    29




```python
try:
    next(gener)
except StopIteration:
    print('The End')
```

    The End



```python
[x for x in range(20,31) if je_prost(x)]
```




    [23, 29]




```python
faktorijal = lambda N: 1 if N <= 1 else N * faktorijal(N-1)
fak_manji_od_100 = (faktorijal(f) for f in range(100) if faktorijal(f) < 100)
```


```python
set(prosti(100)) & set(fak_manji_od_100)
```




    {2}




```python

```

# Generators

## yield


```python
# Create a generator function that yields numbers from 1 to 10
def numbers():
#     for i in range(1, 11):
#         yield i
    yield from range(1,11)
        

# Create a generator function that yields numbers from 11 to 20
def more_numbers():
    for i in range(11, 21):
        yield i
```


```python
numbers()
```




    <generator object numbers at 0x7f9c942e8040>




```python
next(numbers())
```




    1




```python
next(numbers())
```




    1




```python
iterator = numbers()
```


```python
next(iterator), next(iterator), next(iterator), next(iterator), next(iterator), next(iterator),
```




    (1, 2, 3, 4, 5, 6)




```python
next(iterator), next(iterator), next(iterator), next(iterator)
```




    (7, 8, 9, 10)




```python
try:
    next(iterator)
except StopIteration as e:
    print('Kraj')
```

    Kraj


## yield from


```python
def flatten(data):
    for line in data:
        for x in line:
            if x > 0:
                yield x
                
flatten = lambda data: (x for line in data for x in line if x > 0)
```


```python
def is_prime(n):

    def is_prime_re(k,coprime):
        if k%coprime == 0:
            yield False
        if k < coprime**2:
            yield True
        yield from is_prime_re(k,coprime+2)
    
    if n < 2: 
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    return next(is_prime_re(n, 3))
```


```python
try:
    is_prime(62_710_561)
except RecursionError as e:
    print(e)
```

    maximum recursion depth exceeded in comparison



```python
# Create a generator function that delegates to the numbers and more_numbers
# generator functions
def all_numbers():
    yield from numbers()
    yield from more_numbers()

# Call the all_numbers generator function and print the values it yields
for n in all_numbers():
    print(n)
```

    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    15
    16
    17
    18
    19
    20


# Context Manager


```python
class FileContextManager:
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, type, value, traceback):
        self.file.close()

# Use the FileContextManager with the with statement
with FileContextManager("my_file.txt", "w") as file:
    file.write("Hello, world!")
    
def upisi(fajl,text):
    with FileContextManager(fajl,'w') as file:
        file.write(text)
```

# Callable (typing)


```python
# Import the Callable type from the typing module
from typing import Callable

def apply_function(func: Callable[[int], int], arg: int) -> int:
    # Apply the function to the argument and return the result
    return func(arg)

# Define a simple function that adds 1 to its argument
def add_one(x: int) -> int:
    return x + 1

# Call the apply_function function with the add_one function and the argument 10
result = apply_function(add_one, 10)

# Print the result
print(result) # 11

```

    11


# mypy


```python
upisi('apply_function.py','''
# Import the Callable type from the typing module
from typing import Callable

def apply_function(func: Callable[[int], int], arg: int) -> int:
  # Apply the function to the argument and return the result
  return func(arg)

# Define a simple function that adds 1 to its argument
def add_one(x: int) -> int:
  return x + 1

# Call the apply_function function with the add_one function and the argument 10
result = apply_function(add_one, 10)

# Print the result
print(result) # 11
''')
```


```python
!python3 -m pip install mypy > /dev/null && echo ':)' || echo ':('
```

    :)



```python
!mypy apply_function.py
```

    [1m[32mSuccess: no issues found in 1 source file[m


# nbqa
for mypy in jupyter notebook


```python
!python3 -m pip install -U nbqa > /dev/null && echo ':)' || echo ':('
```

    :)



```python
!nbqa mypy "FPython.ipynb"
```

    too long output :(


# Optional (typing)


```python
# Import the Optional type from the typing module
from typing import Optional

def sum_numbers(a: int, b: int, c: Optional[int] = None) -> int:
  # If c is not provided, set it to 0
  if c is None:
    c = 0

  # Return the sum of the numbers
  return a + b + c

# Call the sum_numbers function with two arguments
result = sum_numbers(1, 2)

# Print the result
print(result) # 3

# Call the sum_numbers function with three arguments
result = sum_numbers(1, 2, 3)

# Print the result
print(result) # 6

```

    3
    6


# sum, len, zip


```python
#len implementation
len2 = lambda samples: sum(1 for x in samples) # or len(data)
```


```python
zip, zip_longest, zip_equal, zip_broadcast, zip_offset
```




    (zip,
     itertools.zip_longest,
     <function more_itertools.more.zip_equal(*iterables)>,
     <function more_itertools.more.zip_broadcast(*objects, scalar_types=(<class 'str'>, <class 'bytes'>), strict=False)>,
     <function more_itertools.more.zip_offset(*iterables, offsets, longest=False, fillvalue=None)>)




```python
pairs = zip(
    [1,2,3],
    [4,5,6]
)
sum(p1*p2 for p1,p2 in pairs)
```




    32



# groupby (itertools)


```python
# First, we define a named tuple called "Person" to represent a person
# It has two fields: "name" and "age"
Person = namedtuple("Person", ["name", "age"])

# Then, we create a list of Person instances
people = [Person("Alice", 25), Person("Bob", 30), Person("Charlie", 25), Person("Dave", 28)]

by_age = lambda x: x.age

# We use the `sorted` function to sort the people by their age
sorted_people = reversed(sorted(people, key=by_age))

# We use the `itertools.groupby` function to group the people
# by their age
groups = [(age, list(group)) for age, group in groupby(sorted_people, key=by_age)]

# We print the groups
for age, group in groups:
    print(f"Age {age}: {group}")

```

    Age 30: [Person(name='Bob', age=30)]
    Age 28: [Person(name='Dave', age=28)]
    Age 25: [Person(name='Charlie', age=25), Person(name='Alice', age=25)]


# data model
- `__init__`
- `__repr__`
- `__doc__`
- `__add__`


```python
class X:
    def __init__(self, value):
        self.__value = value
        
    def __add__(self, other):
        return self.__value + other.__value
    
    def __eq__(self, other):
        return self.__value == other.__value
    
a = X(2)
b = X(5)
a + b
```




    7




```python
old_add = X.__add__
```


```python
def new_add (self, other):
    return 100 + self._X__value + other._X__value
    
    #print(dir(self))
    #return old_add(self,other)

X.__add__ = new_add
```


```python
a + b
```




    107




```python
old_bc = __build_class__
```


```python
def my_bc(*a,**kw):
    print(f'BuildClass => {a= }, {kw= }')
    return old_bc(*a,**kw)

import builtins
builtins.__build_class__ = my_bc
```


```python
class Test():
    def __init__():
        print('init HERE')
```

    BuildClass => a= (<function Test at 0x7f9c942dfa30>, 'Test'), kw= {}



```python
builtins.__build_class__ = old_bc
```


```python
def time_it(n=2):
    def inner(f):
        def wrapper(*a,**kw):
            from time import time
            start = time()
            for _ in range(n):
                ret = f(*a,**kw)
            end = time()
            print(f'function {f.__code__.co_name} finished in {end-start}')
            return ret
        return wrapper
    return inner

@time_it(2)
def add(a,b=10):
    return a+b

@time_it(5)
def sub(a,b=10):
    return a+b

print(f'{add(1,5)= }')
print(f'{add(1)= }')
print(f'{add(1e50,2e50)= }')
print(f'{sub(1,5)= }')
print(f'{sub(1)= }')
print(f'{sub(1e50,2e50)= }')
```

    function add finished in 6.67572021484375e-06
    add(1,5)= 6
    function add finished in 4.291534423828125e-06
    add(1)= 11
    function add finished in 1.9073486328125e-06
    add(1e50,2e50)= 3.0000000000000002e+50
    function sub finished in 3.814697265625e-06
    sub(1,5)= 6
    function sub finished in 1.430511474609375e-06
    sub(1)= 11
    function sub finished in 1.6689300537109375e-06
    sub(1e50,2e50)= 3.0000000000000002e+50



```python
from contextlib import contextmanager

@contextmanager
def temp_table(db_file_path='tmp.db'):
    from sqlite3 import connect
    with connect(db_file_path) as conn:
        cur = conn.cursor()
        try:
            cur.execute('create table tmp(x int)')
            yield cur
        finally:
            cur.execute('drop table tmp')

with temp_table() as cur:
    cur.execute('insert into tmp (x) values(1)')
    cur.execute('insert into tmp (x) values(2)')
    cur.execute('insert into tmp (x) values(3)')
    for row in cur.execute('select x from tmp'):
        print(row)
    print('---')
    _ = [print(row) for row in cur.execute('select sum(x) from tmp')]
```

    (1,)
    (2,)
    (3,)
    ---
    (6,)


#### example echo


```python
# imperative version of "echo()"
def echo_IMP():
    while 1:
        x = input("IMP -- ")
        if x == 'quit':
            break
        else:
            print(x)

# echo_IMP()

# FP version of "echo()"
def identity_print(x): # "identity with side-effect"
    print(x)
    return x
echo_FP = lambda: identity_print(input("FP -- "))=='quit' or echo_FP()
# echo_FP()
```

# cache / lru_cache (functools)


```python
HF = {}
def faktorial(N):
    if N <= 1:
        return 1
    if N not in HF:
        HF[N] = N * faktorial(N-1)
    return HF[N]
```


```python
faktorijal1 = lambda n: n*faktorijal1(n-1) if n else 1
faktorijal2 = cache(faktorijal1)

@lru_cache(maxsize=1_000_000)
def factorial(n):
    return n*faktorijal(n-1) if n>0 else 1
```


```python
%timeit faktorijal1(300)
```

    44.5 µs ± 4.72 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)



```python
%timeit faktorijal2(300)
```

    62.7 ns ± 0.465 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)


### cached_property (functools)


```python
class Student:    
    def __init__(self, name:str, test_scores:list[int]):
        self.name = name
        self.test_scores = test_scores
    
    def __repr__(self):
        return f'Student(name={self.name})'
    
    #@property
    @cached_property #Is this a good thing to do in this particular example?
    def mean_test_score(self):
        return sum(x for x in self.test_scores) / len(self.test_scores)
    

```


```python
cached_property
```




    functools.cached_property



### Example: Timed LRU Cache
application e.g. RSS feed ([see references](https://realpython.com/lru-cache-python/))


```python
from datetime import datetime, timedelta

def timed_lru_cache(seconds: int, maxsize: int = 128):

    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.lifetime = timedelta(seconds=seconds)
        func.expiration = datetime.utcnow() + func.lifetime

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expiration:
                func.cache_clear()
                func.expiration = datetime.utcnow() + func.lifetime
            return func(*args, **kwargs)
        
        return wrapped_func
    
    return wrapper_cache
```

# Counter (collections)

Which digits appears how many times in numbers divisible by three and less than 1000?


```python
Counter(chain.from_iterable(str(num) for num in range(3,1000,3)))
```




    Counter({'3': 102,
             '6': 102,
             '9': 102,
             '1': 99,
             '2': 99,
             '5': 99,
             '8': 99,
             '4': 99,
             '7': 99,
             '0': 63})



# Totall Ordering (functools)


```python
class MyClass:
    def __init__(self, value):
        self.value = value

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.value <= other.value

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value

    def __gt__(self, other):
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value
```


```python
@total_ordering
class MyClass:
    def __init__(self, value):
        self.value = value

    def __lt__(self, other):
        return self.value < other.value
    
    def __eq__(self, other):
        return self.value == other.value
```


```python
help(MyClass(2).__gt__)
```

    Help on method __gt__ in module functools:
    
    __gt__(other, NotImplemented=NotImplemented) method of __main__.MyClass instance
        Return a > b.  Computed by @total_ordering from (not a < b) and (a != b).
    


# Example: Tic Tac Toe 
check [this code](https://github.com/simonwardjones/pydata-talk-2022/blob/main/code/tic_tac_toe.py)


```python
from webbrowser import open
open('https://github.com/simonwardjones/pydata-talk-2022/blob/main/code/tic_tac_toe.py')
```




    True



Some function programing repated third-part libs:
- Pyrsistent: https://pyrsistent.readthedocs.io/en/latest/intro.html
- PyToolz: https://hypothesis.readthedocs.io/en/latest/
- Hypothesis: https://hypothesis.readthedocs.io/en/latest/
- More Itertools: https://more-itertools.readthedocs.io/en/stable/

# Reference:
- Official documentaion: 
  https://docs.python.org/3/library/functional.html
- more-itertools documentation: https://more-itertools.readthedocs.io/en/stable/index.html
- Steven Lott - Functional Python Programming: https://www.amazon.com/gp/product/B0788WZRKZ
- Functional Programming in Python - O’Reilly (2015) by David Mertz: https://www.oreilly.com/library/view/functional-programming-in/9781492048633/
- list of dunder methods: https://docs.python.org/3/reference/datamodel.html
- Examples:
    - timed lru cache: https://realpython.com/lru-cache-python/
    - Tic Tac Toe: https://github.com/simonwardjones/pydata-talk-2022/blob/main/code/tic_tac_toe.py
- youtube videos:
    - https://www.youtube.com/watch?v=cKPlPJyQrt4
    - https://www.youtube.com/watch?v=ypApmOoCRSc
